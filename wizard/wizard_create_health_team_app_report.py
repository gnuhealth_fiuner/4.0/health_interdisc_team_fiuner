# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2015 Luis Falcon <falcon@gnu.org>
#    Copyright (C) 2011-2015 GNU Solidario <health@gnusolidario.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime, timedelta
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateTransition, StateView, Button,\
    StateAction
from trytond.transaction import Transaction
from trytond.pool import Pool
import calendar

__all__ = ['HealthTeamAppointmentReportStart',
           'HealthTeamAppointmentReportWizard']


class HealthTeamAppointmentReportStart(ModelView):
    'Request Prescription Batch Start'
    __name__ = 'gnuhealth.health.team.app.report.start'

    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date', required=True)
    confirmed = fields.Boolean('Confirmed')
    done = fields.Boolean('Done')

    @staticmethod
    def default_confirmed():
        return True

    @staticmethod
    def default_done():
        return True

    @staticmethod
    def default_start_date():
        first_day = (datetime.now().replace(day=1)).date()
        return first_day

    @staticmethod
    def default_end_date():
        month_range = calendar.monthrange(datetime.now().year,datetime.now().month)[1]
        last_day = (datetime.now().replace(day=month_range)).date()
        return last_day


class HealthTeamAppointmentReportWizard(Wizard):
    'Request Prescription Batch'
    __name__ = 'gnuhealth.health.team.app.report.wizard'

    start = StateView('gnuhealth.health.team.app.report.start',
        'health_interdisc_team_fiuner.health_team_app_report_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),            
            Button('Imprimir Reporte', 'request', 'tryton-print', default=True),
            ])

    request = StateAction(
        'health_interdisc_team_fiuner.report_appointment_report')

    def fill_data(self):
        midnight = datetime.today().replace(hour=23,minute=59,second=59).time()
        return {
            'start_date': datetime.combine(self.start.start_date, datetime.min.time()),
            'end_date': datetime.combine(self.start.end_date, midnight),
            'confirmed': self.start.confirmed,
            'done': self.start.done,
            }

    def do_request(self,action):
        return action, self.fill_data()
