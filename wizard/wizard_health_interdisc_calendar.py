# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#   
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import timedelta, datetime, time
import pytz
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.modules.health_calendar.exceptions import (NoCompanyTimezone,
                                EndDateBeforeStart, PeriodTooLong)
from trytond.modules.health.core import get_institution
from trytond.i18n import gettext

__all__ = ['CreateHealthTeamAppointmentStart','CreateHealthTeamAppointment'] 

class CreateHealthTeamAppointmentStart(ModelView):
    'Create Health Team Appointment Start'
    __name__ = 'gnuhealth.calendar.create.health.team.app.start'
    
    healthTeam = fields.Many2One('gnuhealth.health.team','Health Team', 
        required=True)
    institution = fields.Many2One('gnuhealth.institution','Institution',
        required=True)
    date_start = fields.Date('Start Date', required=True)
    date_end = fields.Date('End Date', required=True)
    time_start = fields.Time('Start Time', required=True, format='%H:%M')    
    monday = fields.Boolean('Monday')
    tuesday = fields.Boolean('Tuesday')
    wednesday = fields.Boolean('Wednesday')
    thursday = fields.Boolean('Thursday')
    friday = fields.Boolean('Friday')
    saturday = fields.Boolean('Saturday')
    sunday = fields.Boolean('Sunday')    

    @staticmethod
    def default_institution():
        return get_institution()


class CreateHealthTeamAppointment(Wizard):
    'Create Health Team Appointment'
    __name__ = 'gnuhealth.calendar.create.health.team.app'

    start = StateView('gnuhealth.calendar.create.health.team.app.start',
        'health_interdisc_team_fiuner.create_health_team_app_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_', 'tryton-ok', default=True),
            ])
    create_ = StateTransition()
    open_ = StateAction('health_interdisc_team_fiuner.action_gnuhealth_health_team_appointment_view')


    def transition_create_(self):
        pool = Pool()
        HealthTeamAppointment = pool.get('gnuhealth.health.team.app')
        Company = pool.get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)
            else:
                raise NoCompanyTimezone(
                    gettext('health_calendar.no_company_timezone')
                        )

        healthTeamAppointments = []

        # Iterate over days
        day_count = (self.start.date_end - self.start.date_start).days + 1
        
        # Validate dates
        if (self.start.date_start and self.start.date_end):
            if (self.start.date_end < self.start.date_start):
                raise EndDateBeforeStart(
                    gettext('health_calendar.msg_end_before_start')
                    )

            if (day_count > 31):
                raise PeriodTooLong(
                    gettext('health_calendar.msg_period_too_long')
                    )
        
        for single_date in (self.start.date_start + timedelta(n)
            for n in range(day_count)):
            if ((single_date.weekday() == 0 and self.start.monday)
            or (single_date.weekday() == 1 and self.start.tuesday)
            or (single_date.weekday() == 2 and self.start.wednesday)
            or (single_date.weekday() == 3 and self.start.thursday)
            or (single_date.weekday() == 4 and self.start.friday)
            or (single_date.weekday() == 5 and self.start.saturday)
            or (single_date.weekday() == 6 and self.start.sunday)):
                # Iterate over time
                dt = datetime.combine(
                    single_date, self.start.time_start)
                dt = timezone.localize(dt)
                dt = dt.astimezone(pytz.utc)                
                healthTeamAppointment = {
                    'healthTeam': self.start.healthTeam.id,                        
                    'institution': self.start.institution.id,
                    'appointment_date': dt,                   
                    'state': 'confirmed',
                    }
                healthTeamAppointments.append(healthTeamAppointment)                    
        if healthTeamAppointments:
            HealthTeamAppointment.create(healthTeamAppointments)
        return 'open_'

    def do_open_(self, action):
        action['pyson_domain'] = [
            ('healthTeam', '=', self.start.healthTeam.id),
            ('appointment_date', '>=',
                datetime.combine(self.start.date_start, time())),
            ('appointment_date', '<=',
                datetime.combine(self.start.date_end, time())),
            ]
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        action['name'] += ' - %s' % (self.start.healthTeam.name,
                                         )
        return action, {}

    def transition_open_(self):
        return 'end'
    
