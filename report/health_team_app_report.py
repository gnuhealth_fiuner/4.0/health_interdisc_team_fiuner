# -*- coding: utf-8 -*-
##############################################################################
#
#    Health Appointment Report
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction

from datetime import date, datetime
from dateutil.relativedelta import relativedelta


__all__ = ['HealthTeamAppReport']

class HealthTeamAppReport(Report):
    __name__ = 'gnuhealth.health.team.app.report'

    @classmethod
    def get_production(cls, age_division, ht, healthTeamApps, gender):
        #get the appointments from a healthTeam (ht)
        appointments = [x for x in healthTeamApps
                        if x.healthTeam.id == ht.id]

        #put the patients age at the time of the appointments
        patients_age = []
        for app in appointments:
            patients_age += [relativedelta(app.appointment_date,x.patient.dob).years
                         for x in app.patients if x.patient.dob and x.patient.gender == gender]

        #count the production on the age range required
        productions = []
        for ad in age_division[:-1]:
            ad_index = age_division.index(ad)
            ad_value = len([x for x in patients_age
                            if (x>=age_division[ad_index]) and (x<age_division[ad_index+1])])
            productions.append(ad_value)
        return productions

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        HealthTeamApp = pool.get('gnuhealth.health.team.app')
        HealthTeam = pool.get('gnuhealth.health.team')

        context = super().get_context(records, data, name)

        start_date = name['start_date']
        context['start_date'] = name['start_date']

        end_date = name['end_date']
        context['end_date'] = name['end_date']

        confirmed = 'confirmed' if name['confirmed'] else ''
        context['confirmed'] = confirmed

        done = 'done' if name['done'] else ''
        context['done'] = done

        states = [confirmed,done]

        healthTeamApps= HealthTeamApp.search([
                            ('appointment_date','>=',start_date),
                            ('appointment_date','<',end_date),
                            ('state','in',states),
                        ])

        healthTeams = HealthTeam.search([
                            ('id','in',[x.healthTeam.id for x in healthTeamApps]),
                        ])

        context['healthTeams'] = {}
        age_division = [0,1,2,5,10,15,50,300]

        total_ad = {}
        total_ad['male'] = {str(x):0 for x in age_division}
        total_ad['female'] = {str(x):0 for x in age_division}
        total_ad['total'] = {str(x):0 for x in age_division}
        total = 0
        for ht in healthTeams:
            context['healthTeams'][ht.name] = {}
            productions_male = cls.get_production(age_division,ht,healthTeamApps,'m')
            productions_female = cls.get_production(age_division,ht,healthTeamApps,'f')           
            context['healthTeams'][ht.name]['Total'] = 0
            for ad in age_division[:-1]:
                index_ad = age_division.index(ad)
                ad_key = 'from'+str(ad)+'to'+str(age_division[index_ad+1])

                ad_value_male = productions_male[index_ad]
                ad_value_female = productions_female[index_ad]
                ad_total = ad_value_male + ad_value_female

                #Set by range, gender and by hp
                context['healthTeams'][ht.name][ad_key+'male'] = ad_value_male
                context['healthTeams'][ht.name][ad_key+'female'] = ad_value_female
                context['healthTeams'][ht.name]['Total'] += ad_total

                #Set total by gender
                total_ad['male'][str(ad)] += ad_value_male
                total_ad['female'][str(ad)] += ad_value_female
                total_ad['total'][str(ad)] += ad_value_male + ad_value_female
                total += ad_value_male + ad_value_female
        context['total_ad'] = total_ad
        context['total'] = total
        return context
