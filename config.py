# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.model import (ModelSQL, ValueMixin, fields)
from trytond import backend
from trytond.pyson import Id
from trytond.pool import Pool, PoolMeta
from trytond.tools.multivalue import migrate_property

__all__ = [
    'GnuHealthSequences',
    'HealthTeamAppointmentFiunerSequence'
    ]

healthTeamAppointment_sequence = fields.Many2One(
        'ir.sequence', 'Health Team Appointment Sequence', required=True,
        domain=[('sequence_type', '=', Id(
            'health_interdisc_team_fiuner', 'seq_type_gnuhealth_health_team_app'))])


class GnuHealthSequences(metaclass=PoolMeta):
    'Standard Sequences for GNU Health'
    __name__ = 'gnuhealth.sequences'

    healthTeamAppointment_sequence = fields.MultiValue(
        healthTeamAppointment_sequence)

    @classmethod
    def default_healthTeamAppointment_sequence(cls, **pattern):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('health_interdisc_team_fiuner',
                                    'seq_gnuhealth_health_team_app')
        except KeyError:
            return None


class _ConfigurationValue(metaclass=PoolMeta):

    _configuration_value_field = None

    @classmethod
    def __register__(cls, module_name):
        exist = backend.TableHandler.table_exist(cls._table)

        super(_ConfigurationValue, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append(cls._configuration_value_field)
        value_names.append(cls._configuration_value_field)
        migrate_property(
            'gnuhealth.sequences', field_names, cls, value_names,
            fields=fields)

class HealthTeamAppointmentFiunerSequence(_ConfigurationValue, ModelSQL, ValueMixin):
    'Health Team Appointment Setup'
    __name__ = 'gnuhealth.sequences.healthTeamAppointment_sequence'

    healthTeamAppointment_sequence = healthTeamAppointment_sequence
    _configuration_value_field = 'healthTeamAppointment_sequence'

    @classmethod
    def check_xml_record(cls, records, values):
        return True
