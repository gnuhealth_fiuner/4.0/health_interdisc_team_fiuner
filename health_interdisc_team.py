# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime, time

from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.modules.health.core import get_institution

__all__ = [
        'HealthTeam',
        'HealthTeamHP',
        'HealthTeamPatient',
        'HealthTeamAppointment',
        'HealthTeamAppointmentHP',
        'HealthTeamAppointmentPatient'
        ]


class HealthTeam(ModelSQL, ModelView):
    'Health Team'
    __name__ = 'gnuhealth.health.team'

    name = fields.Char('Health Team')
    institution =  fields.Many2One(
        'gnuhealth.institution', "Institution",
        help = 'Health Care Institution',
        required=True)
    healthTeamHP = fields.Many2Many(
        'gnuhealth.health.team-gnuhealth.hp',
        'healthTeam', 'hp', "Health Team Professional")
    healthTeamPatient = fields.Many2Many(
            'gnuhealth.health.team-gnuhealth.patient',
            'healthTeam', 'patient', "Health Team Patient"
            )
    monday = fields.Boolean('Monday')
    tuesday = fields.Boolean('Tuesday')
    wednesday = fields.Boolean('Wednesday')
    thursday = fields.Boolean('Thursday')
    friday = fields.Boolean('Friday')
    saturday = fields.Boolean('Saturday')
    sunday = fields.Boolean('Sunday')    
    start_time = fields.Time('Start Time')
    end_time = fields.Time('End Time')
    description = fields.Text('Description')

    @staticmethod
    def default_institution():
        return get_institution()


class HealthTeamHP(ModelSQL, ModelView):
    'Health Team HP'
    __name__ = 'gnuhealth.health.team-gnuhealth.hp'

    healthTeam = fields.Many2One(
        'gnuhealth.health.team', "Health Team",
        ondelete='CASCADE', required=True)
    hp = fields.Many2One(
        'gnuhealth.healthprofessional', "Health Professional",
        ondelete='CASCADE', required=True)


class HealthTeamPatient(ModelSQL, ModelView):
    'Health Team Patient'
    __name__ = 'gnuhealth.health.team-gnuhealth.patient'

    healthTeam = fields.Many2One('gnuhealth.health.team', "Health Team",
                                 ondelete='CASCADE', required=True)
    patient = fields.Many2One('gnuhealth.patient', 'Patient',
                              ondelete='CASCADE', required=True)


class HealthTeamAppointment(ModelSQL, ModelView):
    'Health Team Appointment'
    __name__ = 'gnuhealth.health.team.app'

    name = fields.Char('Appointment ID', readonly=True)
    healthTeam = fields.Many2One(
        'gnuhealth.health.team','Health Team', required=True)

    activity_type = fields.Selection([
        (None, ''),
        ('individual','Actividad Asistencial Individual'),
        ('groupal','Actividad Asistencial Grupal'),
        ('institutional','Actividad Institucional'),
        ('community','Actividad Comunitaria'),
        ],'Tipo de actividad',sort=False, help='Tipo de actividad referido al\n'\
                                                +'servicio/dispositivo correspondiente')
    individual_activity = fields.Selection([
        (None,''),
        ('a1', u'A.1. Entrevista de admisión y orientación'),
        ('a2', u'A.2. Entrevista o consulta'),
        ('a3', u'A.3. Entrevista Familiar'),
        ('a4', u'A.4. Intervención domiciliaria'),
        ('a5', u'A.5. Acompañamiento Terapéutico'),
        ('a6', u'A.6. Interconsulta'),
        ('a7', u'A.7. Intervención de Urgencia'),
        ('a8', u'A.8. Evaluaciones y Contactos para fines administrativos'),
        ('a9', u'A.9. Actividad Inter o Intrainstitucional'),
        ('a10', u'A.10. Reunión de equipo por usuario'),
        ('a11', u'A.11. Otras'),
        ],'Actividad Individual',sort=False,
            states={
                'invisible':Eval('activity_type')!='individual',
                'required':Eval('activity_type')=='individual',
                })
    groupal_activity = fields.Selection([
        (None,''),
        ('b1', u'B.1. Evaluación y orientación de la demanda grupal'),
        ('b2', u'B.2. Grupo Terapéutico'),
        ('b3', u'B.3. Taller'),
        ('b4', u'B.4. Asamblea'),
        ('b5', u'B.5. Otras'),
        ],'Actividad Grupal',sort=False,
            states={
                'invisible':Eval('activity_type')!='groupal',
                'required':Eval('activity_type')=='groupal',
                })
    institutional_activity = fields.Selection([
        (None,''),
        ('c1', u'C.1. Actividad interdisciplinaria e intersectorial'),
        ('c2', u'C.2. Formación del Equipo de Salud Mental'),
        ('c3', u'C.3. Reunión Equipo de Salud Mental'),               
        ('c4', u'C.4. Docencia y Asesoramiento'),
        ('c5', u'C.5. Otras'),
        ],'Actividad Institucional',sort=False,
            states={
                'invisible':Eval('activity_type')!='institutional',
                'required':Eval('activity_type')=='institutional',
                })
    community_activity = fields.Selection([
        (None,''),
        ('d1', u'D.1. Actividad interdisciplinaria e intersectorial'),
        ('d2', u'D.2. Formación del Equipo de Salud Mental'),
        ('d3', u'D.3. Otras'),
        ],'Actividad Comunitaria',sort=False,
            states={
                'invisible':Eval('activity_type')!='community',
                'required':Eval('activity_type')=='community',
                })
    activity = fields.Function(fields.Char('Activity'),'get_activity')
    health_professionals = fields.One2Many(
        'gnuhealth.health.team.app.hp','healthTeamAppointment',
        'Health Proffesionals')
    patients = fields.One2Many(
        'gnuhealth.health.team.app.pat','healthTeamAppointment',
        'Patients')
    appointment_date = fields.DateTime('Date and Time', required=True)
    institution = fields.Many2One(
        'gnuhealth.institution', 'Institution',
        help = 'Health Care Institution',
        required=True)
    notes = fields.Text('Notes')
    state = fields.Selection([
        (None, ''),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('center_cancelled', 'Cancelled by Health Center'),
        ], 'State', sort=False)
    health_professionals_list = fields.Function(fields.Char('Profesionales de salud'),
                                               'get_health_professionals_list')
    patients_list = fields.Function(fields.Text('Pacientes'),
                                               'get_patients_list')

    def get_health_professionals_list(self, name):
        hp_list = ""
        #if self.health_professionals:
            #for hp in self.health_professionals:
                #if hp.health_professional:
                    #hp_list += hp.health_professional.name.lastname\
                        #+ ", "+ hp.health_professional.name.name + "\n"
        return hp_list

    def get_patients_list(self, name):
        patient_list = ""
        #if self.patients:
            #for pat in self.patients:
                #if pat.patient:
                    #patient_list += pat.patient.name.lastname\
                        #+ ", "+ pat.patient.name.name + "\n"
        return patient_list

    def get_activity(self, name):
        if self.activity_type  == 'individual':
            return self.individual_activity
        elif self.activity_type == 'groupal':
            return self.groupal_activity
        elif self.activity_type == 'institutional':
            return self.institutional_activity
        elif self.activity_type == 'community':
            return self.community_activity
                         
    @fields.depends('healthTeam',
                    'health_professionals',
                    'patients')
    def on_change_healthTeam(self):
        if self.healthTeam:
            if self.healthTeam.healthTeamHP:
                hps = self.healthTeam.healthTeamHP
                for hp in hps:
                    if hp.id not in [x.health_professional.id for x in self.health_professionals]:
                        self.health_professionals +=({
                            'health_professional': hp.id,
                            'state': 'confirmed',
                            }),
            if self.healthTeam.healthTeamPatient:
                patients = self.healthTeam.healthTeamPatient
                for patient in patients:
                    if patient.id not in [x.patient.id for x in self.patients]:
                        self.patients +=({
                            'patient': patient.id,
                            'state': 'confirmed',
                            }),

    @fields.depends('state',
                    'health_professionals',
                    'patients')
    def on_change_state(self):
        if self.health_professionals:
            for hp in self.health_professionals:
                hp.state =\
                    self.state if hp.state == 'confirmed' else hp.state
        if self.patients:
            for patient in self.patients:
                patient.state =\
                    self.state if patient.state == 'confirmed' else patient.state

    @staticmethod
    def default_activity_type():
        return 'individual'

    @staticmethod
    def default_individual_activity():
        return 'a1'

    @staticmethod
    def default_state():
        return 'confirmed'

    @staticmethod
    def default_institution():
        return get_institution()

    @staticmethod
    def default_appointment_date():
        return datetime.now()
    
    @classmethod
    def __setup__(cls):
        super(HealthTeamAppointment, cls).__setup__()
        cls._order.insert(0, ('appointment_date', 'ASC'))

    @classmethod
    def generate_code(cls, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'healthTeamAppointment_sequence', **pattern)
        if sequence:
            return sequence.get()

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if values['state'] == 'confirmed' and not values.get('name'):
                values['name'] = cls.generate_code()
        return super(HealthTeamAppointment, cls).create(vlist)

    @classmethod
    def write(cls, healthTeamAppointments, values):
        Sequence = Pool().get('ir.sequence')
        Config = Pool().get('gnuhealth.sequences')

        for appointment in healthTeamAppointments:
            if values.get('state') == 'confirmed' and not values.get('name'):
                config = Config(1)
                values['name'] = Sequence.get_id(
                    config.healthTeamAppointment_sequence.id)
        return super(HealthTeamAppointment, cls).write(healthTeamAppointments, values)


class HealthTeamAppointmentHP(ModelSQL, ModelView):
    'Health Team Appointment HP'
    __name__ = 'gnuhealth.health.team.app.hp'
    
    healthTeamAppointment = fields.Many2One('gnuhealth.health.team.app',
        'Health Team Appointment', required=True)
    health_professional = fields.Many2One('gnuhealth.healthprofessional',
        'Health Professional', required=True)
    state = fields.Selection([
        (None, ''),        
        ('confirmed', 'Confirmed'),        
        ('done', 'Done'),       
        ('center_cancelled', 'Cancelled by Health Center'),
        ('no_show', 'No show')
        ], 'State', sort=False)
    
    @staticmethod
    def default_state():
        return 'confirmed'
    
    @classmethod
    def __setup__(cls):
        super(HealthTeamAppointmentHP, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('unique_appoint', Unique(t, t.healthTeamAppointment, t.health_professional),
                'The professional is already assigned to this appointment'),
            ]


class HealthTeamAppointmentPatient(ModelSQL, ModelView):
    'Health Team Appointment Patient'
    __name__ = 'gnuhealth.health.team.app.pat'
    
    healthTeamAppointment = fields.Many2One('gnuhealth.health.team.app',
        'Health Team Appointment', required=True)
    patient = fields.Many2One('gnuhealth.patient','Patient', required=True)
    state = fields.Selection([
        (None, ''),        
        ('confirmed', 'Confirmed'),        
        ('done', 'Done'),       
        ('center_cancelled', 'Cancelled by Health Center'),
        ('no_show', 'No show')
        ], 'State', sort=False)
    
    @staticmethod
    def default_state():
        return 'confirmed'    

    @classmethod
    def __setup__(cls):
        super(HealthTeamAppointmentPatient, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('unique_appoint', Unique(t, t.healthTeamAppointment, t.patient),
                'The patient is already assigned to this appointment'),
            ]
